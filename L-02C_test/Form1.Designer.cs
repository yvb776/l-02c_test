﻿namespace L_02C_test
{
	partial class Form1
	{
		/// <summary>
		/// 必要なデザイナー変数です。
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 使用中のリソースをすべてクリーンアップします。
		/// </summary>
		/// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows フォーム デザイナーで生成されたコード

		/// <summary>
		/// デザイナー サポートに必要なメソッドです。このメソッドの内容を
		/// コード エディターで変更しないでください。
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.label1 = new System.Windows.Forms.Label();
			this.btConnect = new System.Windows.Forms.Button();
			this.btReset = new System.Windows.Forms.Button();
			this.btCSQ = new System.Windows.Forms.Button();
			this.cmbPortName = new System.Windows.Forms.ComboBox();
			this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
			this.txtSend = new System.Windows.Forms.TextBox();
			this.btSend = new System.Windows.Forms.Button();
			this.txtRsv = new System.Windows.Forms.TextBox();
			this.btUSBMODEM0 = new System.Windows.Forms.Button();
			this.btCGMM = new System.Windows.Forms.Button();
			this.btCGMI = new System.Windows.Forms.Button();
			this.btCNUM = new System.Windows.Forms.Button();
			this.btCGREG2 = new System.Windows.Forms.Button();
			this.btCGREG = new System.Windows.Forms.Button();
			this.btCIND = new System.Windows.Forms.Button();
			this.btA = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(26, 12);
			this.label1.TabIndex = 1;
			this.label1.Text = "Port";
			// 
			// btConnect
			// 
			this.btConnect.Location = new System.Drawing.Point(584, 12);
			this.btConnect.Name = "btConnect";
			this.btConnect.Size = new System.Drawing.Size(75, 23);
			this.btConnect.TabIndex = 3;
			this.btConnect.Text = "接続";
			this.btConnect.UseVisualStyleBackColor = true;
			this.btConnect.Click += new System.EventHandler(this.btConnect_Click);
			// 
			// btReset
			// 
			this.btReset.Location = new System.Drawing.Point(12, 469);
			this.btReset.Name = "btReset";
			this.btReset.Size = new System.Drawing.Size(95, 23);
			this.btReset.TabIndex = 4;
			this.btReset.Text = "L-02Cのリセット";
			this.btReset.UseVisualStyleBackColor = true;
			this.btReset.Click += new System.EventHandler(this.btReset_Click);
			// 
			// btCSQ
			// 
			this.btCSQ.Location = new System.Drawing.Point(12, 440);
			this.btCSQ.Name = "btCSQ";
			this.btCSQ.Size = new System.Drawing.Size(75, 23);
			this.btCSQ.TabIndex = 5;
			this.btCSQ.Text = "CSQ";
			this.btCSQ.UseVisualStyleBackColor = true;
			this.btCSQ.Click += new System.EventHandler(this.btCSQ_Click);
			// 
			// cmbPortName
			// 
			this.cmbPortName.FormattingEnabled = true;
			this.cmbPortName.Location = new System.Drawing.Point(47, 13);
			this.cmbPortName.Name = "cmbPortName";
			this.cmbPortName.Size = new System.Drawing.Size(121, 20);
			this.cmbPortName.TabIndex = 6;
			// 
			// serialPort1
			// 
			this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
			// 
			// txtSend
			// 
			this.txtSend.Location = new System.Drawing.Point(12, 47);
			this.txtSend.Name = "txtSend";
			this.txtSend.Size = new System.Drawing.Size(541, 19);
			this.txtSend.TabIndex = 7;
			// 
			// btSend
			// 
			this.btSend.Location = new System.Drawing.Point(584, 45);
			this.btSend.Name = "btSend";
			this.btSend.Size = new System.Drawing.Size(75, 23);
			this.btSend.TabIndex = 8;
			this.btSend.Text = "送信";
			this.btSend.UseVisualStyleBackColor = true;
			this.btSend.Click += new System.EventHandler(this.btSend_Click);
			// 
			// txtRsv
			// 
			this.txtRsv.AcceptsReturn = true;
			this.txtRsv.AcceptsTab = true;
			this.txtRsv.Location = new System.Drawing.Point(12, 74);
			this.txtRsv.Multiline = true;
			this.txtRsv.Name = "txtRsv";
			this.txtRsv.ReadOnly = true;
			this.txtRsv.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
			this.txtRsv.Size = new System.Drawing.Size(647, 360);
			this.txtRsv.TabIndex = 9;
			// 
			// btUSBMODEM0
			// 
			this.btUSBMODEM0.Location = new System.Drawing.Point(113, 469);
			this.btUSBMODEM0.Name = "btUSBMODEM0";
			this.btUSBMODEM0.Size = new System.Drawing.Size(97, 23);
			this.btUSBMODEM0.TabIndex = 10;
			this.btUSBMODEM0.Text = "USBMODEM=0";
			this.btUSBMODEM0.UseVisualStyleBackColor = true;
			this.btUSBMODEM0.Click += new System.EventHandler(this.btUSBMODEM0_Click);
			// 
			// btCGMM
			// 
			this.btCGMM.Location = new System.Drawing.Point(584, 469);
			this.btCGMM.Name = "btCGMM";
			this.btCGMM.Size = new System.Drawing.Size(75, 23);
			this.btCGMM.TabIndex = 11;
			this.btCGMM.Text = "CGMM";
			this.btCGMM.UseVisualStyleBackColor = true;
			this.btCGMM.Click += new System.EventHandler(this.btCGMM_Click);
			// 
			// btCGMI
			// 
			this.btCGMI.Location = new System.Drawing.Point(584, 440);
			this.btCGMI.Name = "btCGMI";
			this.btCGMI.Size = new System.Drawing.Size(75, 23);
			this.btCGMI.TabIndex = 12;
			this.btCGMI.Text = "CGMI";
			this.btCGMI.UseVisualStyleBackColor = true;
			this.btCGMI.Click += new System.EventHandler(this.btCGMI_Click);
			// 
			// btCNUM
			// 
			this.btCNUM.Location = new System.Drawing.Point(503, 469);
			this.btCNUM.Name = "btCNUM";
			this.btCNUM.Size = new System.Drawing.Size(75, 23);
			this.btCNUM.TabIndex = 13;
			this.btCNUM.Text = "CNUM";
			this.btCNUM.UseVisualStyleBackColor = true;
			this.btCNUM.Click += new System.EventHandler(this.btCNUM_Click);
			// 
			// btCGREG2
			// 
			this.btCGREG2.Location = new System.Drawing.Point(174, 440);
			this.btCGREG2.Name = "btCGREG2";
			this.btCGREG2.Size = new System.Drawing.Size(75, 23);
			this.btCGREG2.TabIndex = 14;
			this.btCGREG2.Text = "CGREG=2";
			this.btCGREG2.UseVisualStyleBackColor = true;
			this.btCGREG2.Click += new System.EventHandler(this.btCGREG2_Click);
			// 
			// btCGREG
			// 
			this.btCGREG.Location = new System.Drawing.Point(255, 440);
			this.btCGREG.Name = "btCGREG";
			this.btCGREG.Size = new System.Drawing.Size(75, 23);
			this.btCGREG.TabIndex = 15;
			this.btCGREG.Text = "CGREG?";
			this.btCGREG.UseVisualStyleBackColor = true;
			this.btCGREG.Click += new System.EventHandler(this.btCGREG_Click);
			// 
			// btCIND
			// 
			this.btCIND.Location = new System.Drawing.Point(93, 440);
			this.btCIND.Name = "btCIND";
			this.btCIND.Size = new System.Drawing.Size(75, 23);
			this.btCIND.TabIndex = 16;
			this.btCIND.Text = "CIND?";
			this.btCIND.UseVisualStyleBackColor = true;
			this.btCIND.Click += new System.EventHandler(this.btCIND_Click);
			// 
			// btA
			// 
			this.btA.Location = new System.Drawing.Point(216, 469);
			this.btA.Name = "btA";
			this.btA.Size = new System.Drawing.Size(75, 23);
			this.btA.TabIndex = 17;
			this.btA.Text = "A/";
			this.btA.UseVisualStyleBackColor = true;
			this.btA.Click += new System.EventHandler(this.btA_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(671, 504);
			this.Controls.Add(this.btA);
			this.Controls.Add(this.btCIND);
			this.Controls.Add(this.btCGREG);
			this.Controls.Add(this.btCGREG2);
			this.Controls.Add(this.btCNUM);
			this.Controls.Add(this.btCGMI);
			this.Controls.Add(this.btCGMM);
			this.Controls.Add(this.btUSBMODEM0);
			this.Controls.Add(this.txtRsv);
			this.Controls.Add(this.btSend);
			this.Controls.Add(this.txtSend);
			this.Controls.Add(this.cmbPortName);
			this.Controls.Add(this.btCSQ);
			this.Controls.Add(this.btReset);
			this.Controls.Add(this.btConnect);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "L-02C_test";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btConnect;
		private System.Windows.Forms.Button btReset;
		private System.Windows.Forms.Button btCSQ;
		private System.Windows.Forms.ComboBox cmbPortName;
		private System.IO.Ports.SerialPort serialPort1;
		private System.Windows.Forms.TextBox txtSend;
		private System.Windows.Forms.Button btSend;
		private System.Windows.Forms.TextBox txtRsv;
		private System.Windows.Forms.Button btUSBMODEM0;
		private System.Windows.Forms.Button btCGMM;
		private System.Windows.Forms.Button btCGMI;
		private System.Windows.Forms.Button btCNUM;
		private System.Windows.Forms.Button btCGREG2;
		private System.Windows.Forms.Button btCGREG;
		private System.Windows.Forms.Button btCIND;
		private System.Windows.Forms.Button btA;
	}
}


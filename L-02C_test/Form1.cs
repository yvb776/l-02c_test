﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace L_02C_test
{
	public partial class Form1 : Form
	{
		private delegate void Delegate_RcvDataToTextBox(string data);

		public Form1()
		{
			InitializeComponent();
		}

		private void btCSQ_Click(object sender, EventArgs e)
		{
			//! シリアルポートをオープンしていない場合、処理を行わない.
			if (serialPort1.IsOpen == false)
			{
				return;
			}
			
			//送信する構文を生成
			String data = "AT+CSQ" + Environment.NewLine;

			try
			{
				//! シリアルポートからテキストを送信する.
				serialPort1.Write(data);

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btUSBMODEM0_Click(object sender, EventArgs e)
		{
			//! シリアルポートをオープンしていない場合、処理を行わない.
			if (serialPort1.IsOpen == false)
			{
				return;
			}

			//送信する構文を生成
			String data = "AT%USBMODEM=0" + Environment.NewLine;

			try
			{
				//! シリアルポートからテキストを送信する.
				serialPort1.Write(data);

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btReset_Click(object sender, EventArgs e)
		{
			//! シリアルポートをオープンしていない場合、処理を行わない.
			if (serialPort1.IsOpen == false)
			{
				return;
			}

			//送信する構文を生成
			String data = "AT%USBMODEM=0+CFUN=0" + Environment.NewLine;

			try
			{
				//! シリアルポートからテキストを送信する.
				serialPort1.Write(data);

				if (serialPort1.IsOpen == true)
				{
					//! シリアルポートをクローズする.
					serialPort1.Close();

					//! ボタンの表示を[切断]から[接続]に変える.
					btConnect.Text = "接続";

					MessageBox.Show("切断しました。再起動終了後再接続して下さい。");
				}

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btConnect_Click(object sender, EventArgs e)
		{
			if (serialPort1.IsOpen == true)
			{
				//! シリアルポートをクローズする.
				serialPort1.Close();

				//! ボタンの表示を[切断]から[接続]に変える.
				btConnect.Text = "接続";
			}
			else
			{
				//! オープンするシリアルポートをコンボボックスから取り出す.
				serialPort1.PortName = cmbPortName.SelectedItem.ToString();

				serialPort1.BaudRate = 9600;

				//! データビットをセットする. (データビット = 8ビット)
				serialPort1.DataBits = 8;

				//! パリティビットをセットする. (パリティビット = なし)
				serialPort1.Parity = Parity.None;

				//! ストップビットをセットする. (ストップビット = 1ビット)
				serialPort1.StopBits = StopBits.One;

				//! フロー制御をコンボボックスから取り出す.
				serialPort1.Handshake = Handshake.None;

				//! 文字コードをセットする.
				serialPort1.Encoding = Encoding.ASCII;

				try
				{
					//! シリアルポートをオープンする.
					serialPort1.Open();

					//! ボタンの表示を[接続]から[切断]に変える.
					btConnect.Text = "切断";

					//エコーバックの無効化
					String data = "ATE0" + Environment.NewLine;

					try
					{
						//! シリアルポートからテキストを送信する.
						serialPort1.Write(data);

					}
					catch (Exception ex)
					{
						MessageBox.Show(ex.Message);
					}

				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
				}
			}
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			string[] PortList = SerialPort.GetPortNames();

			cmbPortName.Items.Clear();

			foreach (string PortName in PortList)
			{
				cmbPortName.Items.Add(PortName);
			}
			if (cmbPortName.Items.Count > 0)
			{
				cmbPortName.SelectedIndex = 0;
			}

		}

		private void btSend_Click(object sender, EventArgs e)
		{
			//! シリアルポートをオープンしていない場合、処理を行わない.
			if (serialPort1.IsOpen == false)
			{
				return;
			}
			//! テキストボックスから、送信するテキストを取り出す.
			String data = txtSend.Text + Environment.NewLine;

			//! 送信するテキストがない場合、データ送信は行わない.
			if (string.IsNullOrEmpty(data) == true)
			{
				return;
			}

			try
			{
				//! シリアルポートからテキストを送信する.
				serialPort1.Write(data);

				//! 送信データを入力するテキストボックスをクリアする.
				txtSend.Clear();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
		{
			//! シリアルポートをオープンしていない場合、処理を行わない.
			if (serialPort1.IsOpen == false)
			{
				return;
			}

			try
			{
				//! 受信データを読み込む.
				string data = serialPort1.ReadExisting();

				//! 受信したデータをテキストボックスに書き込む.
				Invoke(new Delegate_RcvDataToTextBox(WriteTxt), new Object[] { data });
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		void WriteTxt(string data)
		{
			txtRsv.Text = data;
		}

		private void btCGMM_Click(object sender, EventArgs e)
		{
			//! シリアルポートをオープンしていない場合、処理を行わない.
			if (serialPort1.IsOpen == false)
			{
				return;
			}

			//送信する構文を生成
			String data = "AT+CGMM" + Environment.NewLine;

			try
			{
				//! シリアルポートからテキストを送信する.
				serialPort1.Write(data);

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btCGMI_Click(object sender, EventArgs e)
		{
			//! シリアルポートをオープンしていない場合、処理を行わない.
			if (serialPort1.IsOpen == false)
			{
				return;
			}

			//送信する構文を生成
			String data = "AT+CGMI" + Environment.NewLine;

			try
			{
				//! シリアルポートからテキストを送信する.
				serialPort1.Write(data);

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btCNUM_Click(object sender, EventArgs e)
		{
			//! シリアルポートをオープンしていない場合、処理を行わない.
			if (serialPort1.IsOpen == false)
			{
				return;
			}

			//送信する構文を生成
			String data = "AT+CNUM" + Environment.NewLine;

			try
			{
				//! シリアルポートからテキストを送信する.
				serialPort1.Write(data);

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btCGREG_Click(object sender, EventArgs e)
		{
			//! シリアルポートをオープンしていない場合、処理を行わない.
			if (serialPort1.IsOpen == false)
			{
				return;
			}

			//送信する構文を生成
			String data = "AT+CGREG?" + Environment.NewLine;

			try
			{
				//! シリアルポートからテキストを送信する.
				serialPort1.Write(data);

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btCGREG2_Click(object sender, EventArgs e)
		{
			//! シリアルポートをオープンしていない場合、処理を行わない.
			if (serialPort1.IsOpen == false)
			{
				return;
			}

			//送信する構文を生成
			String data = "AT+CGREG=2" + Environment.NewLine;

			try
			{
				//! シリアルポートからテキストを送信する.
				serialPort1.Write(data);

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btCIND_Click(object sender, EventArgs e)
		{
			//! シリアルポートをオープンしていない場合、処理を行わない.
			if (serialPort1.IsOpen == false)
			{
				return;
			}

			//送信する構文を生成
			String data = "AT+CIND?" + Environment.NewLine;

			try
			{
				//! シリアルポートからテキストを送信する.
				serialPort1.Write(data);

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void btA_Click(object sender, EventArgs e)
		{
			//! シリアルポートをオープンしていない場合、処理を行わない.
			if (serialPort1.IsOpen == false)
			{
				return;
			}

			//送信する構文を生成
			String data = "A/";

			try
			{
				//! シリアルポートからテキストを送信する.
				serialPort1.Write(data);

			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}
	}
}
